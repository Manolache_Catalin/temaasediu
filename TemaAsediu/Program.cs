﻿using System;

namespace TemaAsediu
{
    class Program
    {
       static int supravietuitorul(int volumArray,int locPersoana)
        {
            int[] v = new int[volumArray];
            //variabile de suport
            int cont = 0;
            int numberOfZero = 0;
            int lastPerson = 0;
            //incarc array
            for(int i = 0; i < v.Length; i++)
            {
                v[i] = i + 1;
            }
            //iteratia atat timp cat numarul de elemente "nemarcate cu 0" este mai mare decat 1
            while (numberOfZero!=v.Length-1)
            {
                for(int i = 0; i < v.Length; i++)
                {
                    //daca elementul nu este "marcat" incrementam contorul
                    if (v[i] != 0)
                    {
                        cont++;
                    }
                    //daca am ajuns la pozitia de control,marcam elementul cu 0 si aducem contorul la 0 pentru o noua iterare
                    if (cont == locPersoana)
                    {
                        numberOfZero++;
                        cont = 0;
                        v[i] = 0;
                    }
                }
            }
            //caut elementul care nu este 0 si care va fi ultimul element ramas in viata
            for(int i = 0; i < v.Length; i++)
            {
                if (v[i] != 0)
                {
                    lastPerson = v[i];
                    break;
                }
            }
            return lastPerson;
        }
        static void Main(string[] args)
        {
            //citesc dimensiunea vectorului si numarul de locuri pentru "salt"
            int numarPersoane = int.Parse(Console.ReadLine());
            int numarLocuri = int.Parse(Console.ReadLine());
            Console.WriteLine(supravietuitorul(numarPersoane, numarLocuri));
        }
    }
}
